package com.egydea.lab.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.egydea.lab.ActivityFightZone
import com.egydea.lab.R
import com.egydea.lab.model.HuntList

class HuntListAdapter(private val ctx: Context, private val huntList: ArrayList<HuntList>) :
    RecyclerView.Adapter<HuntListAdapter.HuntListHolder>() {

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        int: Int
    ): HuntListHolder {
        return HuntListHolder(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.element_list_model, viewGroup, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(huntListHolder: HuntListHolder, pos: Int) {
        val item: HuntList = huntList[pos]
        huntListHolder.image.setOnClickListener {
            val intent = Intent(ctx, ActivityFightZone::class.java)
            intent.putExtra("hitPoints", item.monsterHP)
            intent.putExtra("monster", item.monsterName)
            intent.putExtra("position", pos)
            ctx.startActivity(intent)
        }

        huntListHolder.hp.text = item.monsterHP.toString() + " hp"
        huntListHolder.image.setImageResource(item.monsterImage)
        huntListHolder.name.text = item.monsterName
        huntListHolder.arena.setImageResource(item.monsterArena)

    }

    override fun getItemCount(): Int {
        return huntList.size
    }

    class HuntListHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.list_name)
        var hp: TextView = view.findViewById(R.id.list_hp)
        var image: ImageView = view.findViewById(R.id.list_image)
        var arena: ImageView = view.findViewById(R.id.list_image_arena)
    }


}
