package com.egydea.lab.model

data class HuntList( val monsterName: String,val monsterHP : Int, val monsterArena: Int, val monsterImage : Int)