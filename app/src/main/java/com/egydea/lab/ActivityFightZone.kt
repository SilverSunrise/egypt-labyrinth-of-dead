package com.egydea.lab

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_monster_hunt_zone.*


class ActivityFightZone : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monster_hunt_zone)
        var hitPoints = intent.getIntExtra("hitPoints", 10)
        hunt_name.text = intent.getStringExtra("monster")
        monster_hp.text = hitPoints.toString()

        monster.setOnClickListener {
            hitPoints--
            monster_hp.text = hitPoints.toString()
            if (hitPoints == 0) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }

        when (intent.getIntExtra("position", 1)) {
            1 -> {
                fight_zone.setBackgroundResource(R.drawable.sphinx_location)
                monster.setImageResource(R.drawable.sphinx_monster)
            }
            2 -> {
                fight_zone.setBackgroundResource(R.drawable.anubis_location)
                monster.setImageResource(R.drawable.anubis_monster)
            }
            0 -> {
                fight_zone.setBackgroundResource(R.drawable.pharaoh_location)
                monster.setImageResource(R.drawable.pharaoh_monster)
            }
        }


    }
}