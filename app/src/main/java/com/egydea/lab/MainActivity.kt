package com.egydea.lab

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.egydea.lab.adapter.HuntListAdapter
import com.egydea.lab.model.HuntList
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val huntList = ArrayList<HuntList>()
        huntList.add(
            HuntList(
                "Pharaoh",
                70,
                R.drawable.pharaoh_location,
                R.drawable.pharaoh_monster
            )
        )
        val hunterAdapter = HuntListAdapter(this, huntList)
        huntList.add(HuntList("Sphinx", 20, R.drawable.sphinx_location, R.drawable.sphinx_monster))
        rv.layoutManager = LinearLayoutManager(this)
        huntList.add(HuntList("Anubis", 45, R.drawable.anubis_location, R.drawable.anubis_monster))
        rv.adapter = hunterAdapter
    }

    override fun onBackPressed() {
        //nothing
    }


}